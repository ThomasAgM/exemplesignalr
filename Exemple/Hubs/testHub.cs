﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exemple.Hubs
{
    public class testHub : Hub
    {
        static ConcurrentDictionary<string, string> dico= new ConcurrentDictionary<string, string>();
        //static List<string> listePseudo = new List<string>();
        public testHub()
        {      
        }

        public async Task HelloWorld(string message)
        {
            await Clients.All.SendAsync("Recevoir",$" Le serveur dit :{message}"); //Return
        }

        public async Task Confirmation()
        {
            await Clients.Caller.SendAsync("Confirme", "Bien reçu over !"); //Return
        }


        public async Task Login(string user)
        {
            string Id = Context.ConnectionId;
            string pseudo;
            if(!dico.TryGetValue(Id,out pseudo))
            {
                dico.TryAdd(Id, user);
            }
            await Clients.Caller.SendAsync("SendMessage", "Vous êtes connecté");
        }


        public async Task SendMessage(string message)
        {
                await Clients.All.SendAsync("SendMessage", message);             
        }
    }
}
